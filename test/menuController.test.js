jest.mock('../src/controllers/menuController');
const menuController = require('../src/controllers/menuController');

it("Get Menu",async() => {
    var request = require('request');
    let result = jest.requireActual(menuController.index());
    expect(result).toEqual({});
})

it("Create Menu",async() => {
    let request = {};
    var menu = {
    	title:"filé agulha",
    	description:"sim, é um pedaço de carne",
    	value:'25.99'
    };
    let result = jest.requireActual(menuController.create(menu));
    expect(result).toEqual({});
})

it("Delete Menu",async() => {
    let request = {};
    var id = 3;
    let result = jest.requireActual(menuController.delete(id));
    expect(result).toEqual({});
})
