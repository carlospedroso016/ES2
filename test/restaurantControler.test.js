jest.mock('../src/controllers/restaurantController');
const restaurantController = require('../src/controllers/restaurantController');

it("Get restaurant",async() => {
    var request = require('request');
    let result = jest.requireActual(restaurantController.index());
    expect(result).toEqual({});
})

it("Create restaurant",async() => {
    let request = {};
    var restaurant = {
    	name:"restaurante teste",
    	email:"restaurante@teste.com.br",
    	city:"foz do iguacu",
    	ur:"PR"
    };
    let result = jest.requireActual(restaurantController.create(restaurant));
    expect(result).toEqual({});
})

