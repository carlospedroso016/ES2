jest.mock('../src/controllers/userController');
const userController = require('../src/controllers/userController');

it("Get user",async() => {
    var request = require('request');
    let result = jest.requireActual(userController.index());
    expect(result).toEqual({});
})

it("Create user",async() => {
    let request = {};
    var user = {
    	name:"usuario teste",
    	email:"usuario@teste.com.br",
    	city:"foz do iguacu",
    	uf:"PR"
    };
    let result = jest.requireActual(userController.create(user));
    expect(result).toEqual({});
})

