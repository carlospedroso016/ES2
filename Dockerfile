FROM node:latest
ADD src /root/src
ADD knexfile.js /root/knexfile.js
ADD package-lock.json /root/package-lock.json
ADD package.json /root/package.json
RUN cd /root && npm install
EXPOSE 3333
CMD cd /root && npm start
